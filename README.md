# Paperspace Changelog

## Prerequisites

- [Node](https://nodejs.org) (^10.16.0)


## Usage

1. Install module dependencies:

```sh
yarn
```

2. To run the dev server:

```sh
yarn dev
```

3. To run a front-end build:

```sh
yarn build
```

4. To run a static build:

```sh
yarn export
```
