import React, { useContext } from 'react';

import ChangelogContext from '../changelog/ChangelogContext';

import styles from './Avatar.css';

function Avatar() {
  const { data, toggleVisible } = useContext(ChangelogContext);

  return (
    <button
      className={styles.button}
      type="button"
      onClick={toggleVisible}
    >
      {data && <img src={data.avatar} className={styles.image} alt="avatar" />}
      <span className={styles.notification}>2</span>
    </button>
  );
}

export default Avatar;
