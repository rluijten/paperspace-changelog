import React, { useContext } from 'react';

import ChangelogContext from '../changelog/ChangelogContext';
import List from '../changelog/List';

import styles from './Modal.css';

function Modal() {
  const { toggleVisible } = useContext(ChangelogContext);

  return (
    <div className={styles.modal}>
      <button className={styles.close} onClick={toggleVisible} type="button" />

      <List />
    </div>
  );
}

export default Modal;
