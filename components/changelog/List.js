import React, { useContext } from 'react';
import ReactMarkdown from 'react-markdown';

import ChangelogContext from './ChangelogContext';

function List() {
  const { data } = useContext(ChangelogContext);

  return (
    <div>
      {data && <ReactMarkdown source={data.content} />}
    </div>
  );
}

export default List;
