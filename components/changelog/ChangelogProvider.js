import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import ChangelogContext from './ChangelogContext';

function ChangelogProvider({ children }) {
  const [data, setData] = useState(null);
  const [visible, setVisible] = useState(false);

  useEffect(() => {
    fetch('https://api.github.com/gists/11387d422d2fba2e123d7425e8a455b3')
      .then(res => res.json())
      .then(json => setData({
        content: json.files['changelog.md'].content,
        avatar: json.owner.avatar_url,
        isOpen: false
      }));
  }, []);

  function toggleVisible() {
    setVisible(bool => !bool);
  }

  return (
    <ChangelogContext.Provider value={{ data, visible, toggleVisible }}>
      {children}
    </ChangelogContext.Provider>
  );
}

ChangelogProvider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node
  ]).isRequired
};

export default ChangelogProvider;
