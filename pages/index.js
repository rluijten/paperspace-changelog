import React, { useContext } from 'react';
import Head from 'next/head';

import ChangelogContext from '../components/changelog/ChangelogContext';

import Avatar from '../components/avatar/Avatar';
import Modal from '../components/modal/Modal';

function Home() {
  const { visible } = useContext(ChangelogContext);

  return (
    <div>
      <Head>
        <title>Paperspace Changelog</title>
        <meta key="viewport" name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>

      <h1 style={{ textAlign: 'center' }}>Paperspace Changelog.</h1>

      <Avatar />
      {visible && <Modal />}
    </div>
  );
}

export default Home;
