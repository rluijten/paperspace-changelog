import 'isomorphic-fetch';

import React from 'react';
import App, { Container } from 'next/app';

import '../styles/index.scss';

import ChangelogProvider from '../components/changelog/ChangelogProvider';

export default class MyApp extends App {
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  render() {
    const { Component, pageProps } = this.props;

    return (
      <Container>
        <ChangelogProvider>
          <Component {...pageProps} />
        </ChangelogProvider>
      </Container>
    );
  }
}
